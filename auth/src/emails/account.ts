import * as nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS
    }
});

export const sendVerificationEmail = async (email: string, name: string, token: string) => {
    const verifyLink = `${process.env.CLIENT_URI}/verify?token=${token}`;

    const htmlBody = `<html lang="en">
        <head>
            <title>Microservices Verify Account</title>
        </head>
        <body style="font-size: 20px">
            <div>
                <h3>Hi ${name},</h3>
                    <p>You're on your way!</p>
                    <p>Let's confirm your email address.</p>
                    <p>By clicking on the following link, you are confirming your email address.</p>
                    <p><a href=${verifyLink}>${verifyLink}</a></p>
                    <br>
                    <p>Cheers!</p>
            </div>
        </body>
    </html>`;

    const mailData = {
        from: 'no-reply@email.com',
        to: email,
        subject: 'Microservices Verify Account ✔',
        html: htmlBody
    };

    transporter.sendMail(mailData, (error, info) => {
        if (error) {
            return console.log(`error: ${error}`);
        }
        console.log(`Message Sent ${info.response}`);
    });
};

export const sendEmailResetLink = async (email: string, name: string, token: string) => {
    const resetLink = `${process.env.CLIENT_URI}/reset?token=${token}`;

    const htmlBody = `<html lang="en">
        <head>
            <title>Microservices Forget Password</title>
        </head>
        <body>
        <div style="font-size: 20px">
            <h3>Hi ${name},</h3>
            <p>Someone requested a new password for your Custex account.</p>
            <p>Kindly use this <a href=${resetLink}>link</a> to reset your password</p>
            <p>If you didn't make this request then you can safely ignore this email :)</p>
            <br>
            <p>Cheers!</p>
        </div>
        </body>
    </html>`;

    const mailData = {
        from: 'no-reply@email.com',
        to: email,
        subject: 'Microservices Reset Password ✔',
        html: htmlBody
    };

    transporter.sendMail(mailData, (error, info) => {
        if (error) {
            return console.log(`error: ${error}`);
        }
        console.log(`Message Sent ${info.response}`);
    });
};

