import mongoose from 'mongoose';
import {Password} from "../services/password";

// An interface that describes the properties
// that are required to create new User
interface UserAttrs {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    verifyToken: string;
    expireVerifyToken: number;
}

// An interface that describes the properties
// that a User Model has
interface UserModel extends mongoose.Model<UserDoc> {
    build(attrs: UserAttrs): UserDoc;
}

// An interface that describes the properties
// that a User (single user) Document has
interface UserDoc extends mongoose.Document {
    verify: number;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}

const userSchema = new mongoose.Schema({
        firstName: {
            type: String,
            required: true
        },
        lastName: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        verify: {
            type: Number,
            default: 0
        },
        verifyToken: {
            type: String,
        },
        expireVerifyToken: {
            type: Date
        },
        resetPasswordToken: {
            type: String,
            default: null
        },
        expirePasswordToken: {
            type: Date,
            default: null
        }
    },
    {
        toJSON: {
            transform(doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                delete ret.__v;
                delete ret.password;
            }
        }
    });

userSchema.pre('save', async function (done) {
    if (this.isModified('password')) {
        const hashed = await Password.toHash(this.get('password'));
        this.set('password', hashed);
    }
    done();
});

userSchema.statics.build = (attrs: UserAttrs) => {
    return new User(attrs);
};

const User = mongoose.model<UserDoc, UserModel>('User', userSchema);

export {User};