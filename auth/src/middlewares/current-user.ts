import {NextFunction, Request, Response} from 'express';
import jwt from 'jsonwebtoken';

interface UserPayload {
    //info: object;  // all User info
    id: string;
    email: string;
}

declare global {
    namespace Express {
        interface Request {
            currentUser?: UserPayload;
        }
    }
}

export const currentUser = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    if (!req.session?.jwt) { // question mark: if req session is defined!!!
        return next();
    }

    try {
        req.currentUser = jwt.verify(
            req.session.jwt,
            process.env.JWT_KEY!
        ) as UserPayload;
    } catch (err) {
    }

    next();
};