import express, {Request, Response} from 'express';
import {body} from "express-validator";
import cryptoRandomString from 'crypto-random-string';

import {User} from "../models/user";
import {BadRequestError} from "../errors/bad-request-error";
import {validateRequest} from "../middlewares/validate-request";
import {sendVerificationEmail} from "../emails/account";

const router = express.Router();

router.post(
    '/api/users/signup',
    [
        body('firstName')
            .trim()
            .notEmpty()
            .escape()
            .withMessage('firstName is required field'),
        body('lastName')
            .trim()
            .notEmpty()
            .escape()
            .withMessage('lastName is required field'),
        body('email')
            .isEmail()
            .normalizeEmail()
            .withMessage('Email must be valid'),
        body('password')
            .trim()
            .isLength({min: 4, max: 20})
            .withMessage('Password must be between 4 and 20 characters')
    ],
    validateRequest,
    async (req: Request, res: Response) => {
        const {email, password, firstName, lastName} = req.body;

        // check if user with that email exist in db
        const existingUser = await User.findOne({email});

        if (existingUser) {
            throw new BadRequestError('Email in use.');
        }

        // create new user
        const verifyToken = cryptoRandomString({length: 64, type: 'base64'});
        const expireVerifyToken = Date.now() + 3600000;
        const user = User.build({
            email,
            password,
            firstName,
            lastName,
            verifyToken,
            expireVerifyToken
        });
        await user.save();

        // send email notification for verify account
        await sendVerificationEmail(email, firstName, verifyToken);

        res.status(201).json({
            message: 'User created!',
            userId: user.id
        });
});

export {router as signUpRouter};