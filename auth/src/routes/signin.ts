import express, {Request, Response} from 'express';
import {body} from "express-validator";
import jwt from 'jsonwebtoken';

import {Password} from "../services/password";
import {validateRequest} from "../middlewares/validate-request";
import {User} from "../models/user";
import {BadRequestError} from "../errors/bad-request-error";

const router = express.Router();

router.post('/api/users/signin',
    [
        body('email')
            .isEmail()
            .withMessage('Email must be valid'),
        body('password')
            .notEmpty()
            .withMessage('You must enter password')
    ],
    validateRequest,
    async (req: Request, res: Response) => {
        const {email, password} = req.body;

        // check credentials
        const existingUser = await User.findOne({email});
        if (!existingUser) {
            throw new BadRequestError('Invalid credentials.');
        }

        // check if user account verify
        if (existingUser.verify == 0) {
            throw new BadRequestError('User account is not verify yet.')
        }

        // check if passwords match
        const passwordsMatch = await Password.compare(
            existingUser.password,
            password
        );
        if (!passwordsMatch) {
            throw new BadRequestError('Invalid credentials....');
        }

        // generate JWT token
        const userJwt = jwt.sign({
                //info: existingUser  // put all User info in JWT
                id: existingUser.id,
                email: existingUser.email
            },
            process.env.JWT_KEY!
        );

        // store it on session object
        req.session = {
            jwt: userJwt
        };

        res.status(200).json({
            message: 'User is now loggedIn to application!',
            userId: existingUser.id
        });
    });

export {router as signInRouter};