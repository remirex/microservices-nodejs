import express, {Request, Response} from 'express';
import {body} from "express-validator";
import cryptoRandomString from 'crypto-random-string';

import {User} from "../models/user";
import {BadRequestError} from "../errors/bad-request-error";
import {validateRequest} from "../middlewares/validate-request";
import {sendEmailResetLink} from "../emails/account";

const router = express.Router();

router.post(
    '/api/users/email',
    [
        body('email')
            .isEmail()
            .withMessage('Email must be valid')
    ],
    validateRequest,
    async (req: Request, res: Response) => {
        const {email} = req.body;

        // check if user with that email exist in db
        const existingUser = await User.findOne({email});

        if (!existingUser) {
            throw new BadRequestError('Email does not exist in db');
        }

        const token = cryptoRandomString({length: 64, type: 'base64'});
        const expire = Date.now() + 3600000;

        existingUser.set({
            resetPasswordToken: token,
            expirePasswordToken: expire
        });

        await existingUser.save();

        // send email notification for reset password
        await sendEmailResetLink(email, existingUser.firstName, token);

        res.status(200).json({
            message: 'Successfully send email with password reset link!',
            userId: existingUser.id
        });
    }
);

export {router as forgotRouter}