import express, {Request, Response} from 'express';
import {body} from "express-validator";

import {User} from "../models/user";
import {BadRequestError} from "../errors/bad-request-error";
import {validateRequest} from "../middlewares/validate-request";
import {Password} from "../services/password";

const router = express.Router();

router.put(
    '/api/users/reset',
    [
        body('token')
            .notEmpty()
            .withMessage('Token is required field'),
        body('newPassword')
            .trim()
            .isLength({min: 4, max: 20})
            .withMessage('Password must be between 4 and 20 characters')
            .custom((value,{req}) => {
                if (value !== req.body.confirmPassword) {
                    // trow error if passwords do not match
                    throw new Error("Passwords don't match");
                } else {
                    return value;
                }
            }),
        body('confirmPassword')
            .notEmpty()
            .withMessage('confirmPassword is required field')
    ],
    validateRequest,
    async (req: Request, res: Response) => {
        const {token, newPassword} = req.body;

        // check if user with that token exist in db and token is not expired
        const user = await User.findOne({resetPasswordToken: token, expirePasswordToken: { $gte: Date.now() }});

        console.log(user);

        if (!user) {
            throw new BadRequestError('Token do not exist in db or expire.');
        }

        user.set({
            resetPasswordToken: null,
            expirePasswordToken: null,
            password: newPassword // we do not need hash password because we set pre SAVE method with hashing password in User model
        });

        await user.save();

        res.status(200).json({
            message: 'Successfully reset password!',
            userId: user.id
        });
    }
);

export {router as resetRouter}