import express, {Request, Response} from 'express';
import {body} from "express-validator";
import {User} from "../models/user";
import {BadRequestError} from "../errors/bad-request-error";
import {validateRequest} from "../middlewares/validate-request";

const router = express.Router();

router.put(
    '/api/users/verify',
    [
        body('token')
            .notEmpty()
            .withMessage('Token is required field')
    ],
    validateRequest,
    async (req: Request, res: Response) => {
        const {token} = req.body;

        // check if user with that token exist in db and token is not expired
        const user = await User.findOne({verifyToken: token, expireVerifyToken: { $gte: Date.now() }});

        if (!user) {
            throw new BadRequestError('Token do not exist in db or expire.');
        }

         user.set({
             verify: 1,
             verifyToken: null,
             expireVerifyToken: null,
         });

        await user.save();

        res.status(200).json({
            message: 'User account is verified!',
            userId: user.id
        });
    }
);

export {router as verifyRouter};