require('dotenv').config();
import express from 'express';
import 'express-async-errors';
import mongoose from 'mongoose';
import {json} from 'body-parser';
import cookieSession from "cookie-session";

import {currentUserRouter} from './routes/current-user'
import {signInRouter} from './routes/signin'
import {signUpRouter} from './routes/signup'
import {signOutRouter} from './routes/signout'
import {verifyRouter} from "./routes/verify";
import {forgotRouter} from "./routes/forgot";
import {resetRouter} from "./routes/reset";
import {errorHandler} from "./middlewares/error-handler";
import {NotFoundError} from "./errors/not-found-error";

const app = express();
app.set('trust proxy', true);
app.use(json());
app.use(
    cookieSession({
        signed: false, // JWT already encrypted
        secure: true
    })
);

app.use(currentUserRouter);
app.use(signInRouter);
app.use(signUpRouter);
app.use(signOutRouter);
app.use(verifyRouter);
app.use(forgotRouter);
app.use(resetRouter);

app.all('*', async () => {
    throw new NotFoundError();
});

app.use(errorHandler);

const start = async () => {
    if (!process.env.JWT_KEY) {
        throw new Error('JWY_KEY must be defined.');
    }

    if (!process.env.MONGO_URI) {
        throw new Error('MONGO_URI must be defined.');
    }

    try {
        await mongoose.connect(process.env.MONGO_URI, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true
        });
        console.log('Connected to MongoDb.');
    } catch (err) {
        console.error(err);
    }

    app.listen(3000, () => {
        console.log('Listening on port 3000.');
    });
};

start().then(() => console.log('Start listening to traffic.'));

