# Microservices with Node JS

Build, deploy, and scale app using Microservices built with Node, MongoDB, Docker and Kubernetes.

![nodejs_docker_kubernetes](img/nodejs.png)

### Requirements

 - Node
 - npm
 - Docker
 - Kubernetes or (Minikube)
 - MongoDB

